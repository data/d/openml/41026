# OpenML dataset: gisette

https://www.openml.org/d/41026

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Isabelle Guyon, Steve Gunn, Asa Ben Hur, Gideon Dror  
**Source**: [LIBSVM](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html)    
**Please cite**:   

GISETTE is a handwritten digit recognition problem. The problem is to separate the highly confusable digits '4' and '9'. This dataset is one of five datasets of the NIPS 2003 feature selection challenge.

The digits have been size-normalized and centered in a fixed-size image of dimension 28x28. The original data were modified for the purpose of the feature selection challenge. In particular, pixels were samples at random in the middle top part of the feature containing the information necessary to disambiguate 4 from 9 and higher order features were created as products of these pixels to plunge the problem in a higher dimensional feature space. We also added a number of distractor features called 'probes' having no predictive power. The order of the features and patterns were randomized. 

Preprocessing: The data set is also available at UCI. Because the labels of testing set are not available, here we use the validation set (gisette_valid.data and gisette_valid.labels) as the testing set. The training data (gisette_train) are feature-wisely scaled to [-1,1]. Then the testing data (gisette_valid) are scaled based on the same scaling factors for the training data.

Difference with version 1: the target feature is now binary, as it should be.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41026) of an [OpenML dataset](https://www.openml.org/d/41026). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41026/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41026/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41026/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

